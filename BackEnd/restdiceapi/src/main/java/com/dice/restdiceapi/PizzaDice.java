package com.dice.restdiceapi;

import java.util.Random;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PizzaDice {
    @CrossOrigin
    @GetMapping("/quaysodice")
    public String hello() {
        // Tạo một số ngẫu nhiên từ 1 đến 6
        int randomNumber = new Random().nextInt(6) + 1;
       String username=" tây đui";

        // Rest Trả về nội dung
       return "Xin chào " + username + ", Số may mắn hôm nay của bạn là: " + randomNumber;
    }

}
