package com.dev.reststringapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
//import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;  
import java.util.Locale;  
import java.time.format.DateTimeFormatter;  
import java.time.ZoneId;  

@RestController
public class CPizzaDateTimeAndPromotion {
    @CrossOrigin
    @GetMapping("/devcamp-pizza")
    public String getDateVietString(){
        DateTimeFormatter dtfVietnam= DateTimeFormatter.ofPattern("EEEE").localizedBy(Locale.forLanguageTag("vi"));
        LocalDate today= LocalDate.now(ZoneId.systemDefault());
        String username="Huy Nguyen 18+";
        return String.format(" Hello "+ username+". Hôm nay %s, mua 1 tặng 1, tính tiền 2", dtfVietnam.format(today));
    }
}
